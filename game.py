import random

class Game():

    def __init__(self, player1, player2) -> None:
        self.players = [player1, player2]
        self.grid = [
            ['-', '-', '-'],
            ['-', '-', '-'],
            ['-', '-', '-']
        ]
        self.launch = True
        self.turn = random.randint(0, 1)

    def getGrid(self):
        return self.grid

    def getPlayers(self):
        return self.players

    def isLaunch(self):
        return self.launch

    def changeTurn(self):
        self.turn = 1 if self.turn == 0 else 0

    def set(self, player, ligne, colonne):
        
        correct_index = [0, 1, 2]
        x = colonne - 1
        y = ligne - 1

        if x not in correct_index:
            return f"\n{colonne} is not correct in column."
        elif y not in correct_index:
            return f"\n{ligne} is not correct in row."
        elif self.grid[y][x] != '-':
            return f'\nA player already play at {colonne}x {ligne}y.. Try again in an empty place !'
        else:

            self.grid[y][x] = player.getSymbole()
            return True

    def isFinish(self):

        for ligne in self.grid:
            for colonne in ligne:
                if colonne == '-':
                    return False

        return True

    def hasWinner(self):

        for i in range(0, len(self.grid)):
            
            temp = ''
            for j in range(0, len(self.grid[i])):
                temp += self.grid[i][j]
            
            if temp == 'OOO':
                return self.players[0]
            elif temp == 'XXX':    
                return self.players[1]

        for i in range(0, len(self.grid)):
            
            temp = ''
            for j in range(0, len(self.grid[i])):
                temp += self.grid[j][i]
            
            if temp == 'OOO':
                return self.players[0]
            elif temp == 'XXX':    
                return self.players[1]

        temp = ''
        for i in range(0, len(self.grid)):
            
            temp += self.grid[i][i]
            
            if temp == 'OOO':
                return self.players[0]
            elif temp == 'XXX':    
                return self.players[1]
            
        temp = ''
        for i in range(0, len(self.grid)):
            temp += self.grid[i][len(self.grid) - (i + 1)]
            
        if temp == 'OOO':
            return self.players[0]
        elif temp == 'XXX':    
            return self.players[1]

        return False

    def printGrid(self):

        toPrint = "\n  1 2 3\n"
        for i in range(0, len(self.grid)):
            toPrint += f"{i+1} {self.grid[i][0]} {self.grid[i][1]} {self.grid[i][2]}\n"

        print(toPrint)