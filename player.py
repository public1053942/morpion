class Player():

    def __init__(self, name, symbole) -> None:
        self.name = name
        self.symbole = symbole

    def getName(self):
        return self.name

    def getSymbole(self):
        return self.symbole