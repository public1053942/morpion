from game import *
from player import *
import os

# Demander les prénoms
game = Game(Player(input("Joueur1 name -> "), 'O'), Player(input("Joueur2 name -> "), 'X'))

game.printGrid()

while game.isLaunch():

    game.changeTurn()
    print(f"{game.players[game.turn].getName()} it's your turn !")

    resultat = ''
    while type(resultat) == str:
        print(resultat)
        resultat = game.set(game.players[game.turn], int(input("Choose a row -> ")), int(input("Choose a column -> ")))

    if type(resultat) == str:
        print(resultat)
    else:
        os.system('cls')
        game.printGrid()

    winner = game.hasWinner()
    if winner:
        game.launch = False
        print(f"The game is end, {winner.getName()} win !")
    elif game.isFinish():
        game.launch = False
        print("The game is end, no one win.")